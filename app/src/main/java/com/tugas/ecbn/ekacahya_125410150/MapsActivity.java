package com.tugas.ecbn.ekacahya_125410150;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    String namasekolah;
    Float lat,lon;
    private static final String TAG_IDMEM = "id";
    private static final String TAG_NAMA = "nama";
    private static final String TAG_LONG = "long";
    private static final String TAG_LAT = "lat";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tiga);

        Intent i = getIntent();
        namasekolah = i.getStringExtra(TAG_NAMA);
        lat = Float.parseFloat(i.getStringExtra(TAG_LAT));
        lon = Float.parseFloat(i.getStringExtra(TAG_LONG));

        LatLng sekolah = new LatLng(lat,lon);

        SupportMapFragment mapFrag = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
        mMap = mapFrag.getMap();
        mMap.setMyLocationEnabled(true);
        mMap.addMarker(new MarkerOptions()
                .position(sekolah)
                .title(namasekolah));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(sekolah)
                .zoom(17)
                .bearing(0)
                .tilt(45)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        Toast.makeText(getBaseContext(), namasekolah+", "+lat+", "+lon,Toast.LENGTH_SHORT).show();
    }

}
